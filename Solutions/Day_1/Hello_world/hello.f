      program main
!$    include 'omp_lib.h'

      print *, "Hello world, I am a serial code!"

!$omp parallel
!$    write (*,*) "Hello world, I am" , omp_get_thread_num(), "of"
!$   &     , omp_get_num_threads()
!$omp end parallel

      end program

Program pi
  !$ use omp_lib
  implicit none

  integer, parameter :: finval = 10000
  double precision :: pi_square, pi_square_acc = 0.d0
  double precision :: factor

  integer:: i, istart, iend
  integer:: thread_id=0, local_size=finval

  logical:: omp_on = .false.

  !$ omp_on = .true.

  !$omp parallel do default(none) &
  !$omp private(thread_id, local_size, i, factor) &
  !$omp reduction(+: pi_square)
  do i= 1, finval
     factor = i
     pi_square = pi_square + 1.0d0/(factor * factor)
  enddo

  print *, 'Pi**2 =', 6.0d0 * pi_square

end Program pi

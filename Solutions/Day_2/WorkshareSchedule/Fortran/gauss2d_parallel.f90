Program gauss2d
  !$ use omp_lib
  implicit none

  ! parameters N and h
  integer, parameter :: nsize    = 44800
  double precision, parameter :: stepsize = 0.0002D0


  double precision :: xpos, ypos
  double precision :: integral, local_integral

  ! summation indices
  integer :: i, j

  integral = 0.0D0

  !$omp parallel default(none) &
  !$omp private(i, j, xpos, ypos, local_integral) shared(integral)

  local_integral = 0.0d0

  !$omp do schedule(static, 100)
  do i = 0, nsize-1
     xpos = i * stepsize
     do j = i+1, nsize-1
        ypos = j * stepsize
        local_integral = local_integral + &
             stepsize*stepsize*exp(-xpos*xpos - ypos*ypos)
     enddo  ! j-loop
  enddo     !i-loop

  !$omp atomic update
  integral = integral + local_integral

  !$omp end parallel

  integral = 8.0D0 * integral

  print *, "Approximate integral:", integral, " for n:", nsize, " and stepsize:", stepsize

end program gauss2d

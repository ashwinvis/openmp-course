Program pi
  use omp_lib
  implicit none

  integer, parameter :: finval = 10000
  double precision :: pi_square, pi_square_acc = 0.d0
  double precision :: factor
  integer:: i, istart, iend, local_size

  integer:: thread_id

  !$omp parallel default(none) &
  !$omp private(thread_id, local_size, i, istart, iend, factor, pi_square) &
  !$omp shared(pi_square_acc)

  pi_square = 0.d0

  thread_id = omp_get_thread_num()
  local_size = finval / omp_get_num_threads()

  istart = 1 + thread_id * local_size
  iend = (thread_id + 1) * local_size

  do i= istart, iend
     factor = i
     pi_square = pi_square + 1.0d0/(factor * factor)
  enddo
  print *, 'thread_id=', thread_id, istart, iend, 'Pi**2 =', 6.0d0 * pi_square

  !$omp atomic update

  ! accumulate partial sums
  pi_square_acc = pi_square_acc + pi_square

  !$omp end parallel

  print *, 'Pi**2 =', 6.0d0 * pi_square_acc

end Program pi
